from django.db import models



class VehicleInfo(models.Model):
    vin = models.CharField(max_length=255, null=True)
    make = models.CharField(max_length=255, null=True)
    model = models.CharField(max_length=255, null=True)
    year = models.CharField(max_length=125, null=True)
    type = models.CharField(max_length=125, null=True)
    color = models.CharField(max_length=125, null=True)
    dimensions = models.CharField(max_length=125, null=True)

    def __str__(self):
        return self.vin

    class Meta:
        db_table = 'vehicle_records'