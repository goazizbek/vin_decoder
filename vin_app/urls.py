from  django.conf.urls import url
from .views import *


urlpatterns = [
    url(r'^vin/decode/', VinDecodeView.as_view()),
    url(r'^vin/list/', VinListView.as_view()),
    url(r'^vin/(?P<vin>[\w]+)/$', VinDetailView.as_view())
]