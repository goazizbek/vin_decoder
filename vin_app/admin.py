from django.contrib import admin

from .models import VehicleInfo

admin.site.register(VehicleInfo)