from vin_app import models
from rest_framework import serializers


class VehicleInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.VehicleInfo
        fields = ['vin', 'make', 'model', 'year', 'type', 'color', 'dimensions']