# Dockerfile

# Pull base image
FROM python:3.7

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /vin_decoder_server

# Set work directory
WORKDIR /vin_decoder_server
COPY requirements.txt /vin_decoder_server/

# Install dependencies
RUN pip install -r requirements.txt

# Copy project
COPY . /vin_decoder_server/