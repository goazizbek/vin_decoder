# VIN Decoder Microservice

To run this app firstly clone this project 

```bash
	git clone https://goazizbek@bitbucket.org/goazizbek/vin_decoder.git
```

### Building locally 
Next step, from your project root, you can download all dependencies of the project with:

```bash
	pip install -r requirements.txt
```

To run locally:

```bash
	python manage.py runserver
```

Your app will be running at ``127.0.0.1:8000`` url. Open Postman and access the ``/vin/decoder/`` endpoint at url.
In Postman body type **vin_code** for key and for value paste this **4T1BF1FK4FU954706** to decode vin. After sending request you should see your decoded vin.