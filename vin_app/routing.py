import requests

def vin_decode(querystring):

    url = "https://a2427895-6989-47d0-ae8c-d2c4f2e3a01b.mock.pstmn.io/vin/check"

    response = requests.request("GET", url, params=querystring)
    return response