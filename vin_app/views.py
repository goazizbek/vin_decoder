from django.http import JsonResponse
from rest_framework import views
from rest_framework import generics
from .routing import vin_decode
from rest_framework import status
from vin_app import models
from vin_app import serializers


class VinDecodeView(views.APIView):

    def get(self, request, *args, **kwargs):
        body = request.data
        vin_code = body['vin_code'] # here you can enter vin in postman i.e vin_code = 2VVBNV541FFD412
        vin_info = vin_decode(querystring=vin_code)
        vin_specification = vin_info.json()

        vin_make = vin_specification['make']
        vin_model = vin_specification['model']
        vin_year = vin_specification['years']
        vin_colors = vin_specification['colors'][0]['options']
        vin_dimensions = vin_specification['engine']
        vinModel = models.VehicleInfo(vin=vin_specification['vin'])
        print(vin_specification['make']['name'])

        if vinModel is not None:
            vinModel.vin = vin_specification['vin']
            vinModel.make = vin_make['name']
            vinModel.model = vin_model['name']
            vinModel.year = vin_year[0]['year']
            vinModel.dimensions = vin_dimensions['name']
            vinModel.type = vin_dimensions['type']
            vinModel.color = vin_colors[0]['name']
            vinModel.save()
            data_format = {
                'vin': vinModel.vin,
                'make': vinModel.make,
                'model': vinModel.model,
                'year': vinModel.year,
                'type': vinModel.type,
                'color': vinModel.color,
                'dimensions': vinModel.dimensions
            }
            return JsonResponse(data_format, status=status.HTTP_200_OK)
        return JsonResponse(vin_info.json(), status=status.HTTP_200_OK)


class VinListView(generics.ListAPIView):
    queryset = models.VehicleInfo.objects.all()
    serializer_class = serializers.VehicleInfoSerializer


class VinDetailView(generics.RetrieveAPIView):
    serializer_class = serializers.VehicleInfoSerializer

    def get_object(self):
        vin = models.VehicleInfo.objects.get(vin=self.kwargs.get('vin'))
        return vin
